from django.contrib import messages, auth
from django.contrib.auth import get_user_model, login, logout
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.views import View

from APP.models import Item, ItemType


def home(request):
    items = Item.objects.raw('select *,count(item_type_id) as count from APP_item group by item_type_id')
    context = {
        'items': items
    }
    return render(request, 'home/index.html',context)


class LoginView(View):

    def get(request):
        return render(request, 'auth/login.html')

    def post(request):
        username = request.POST['username']
        password = request.POST['password']

        if username and password:
            user = auth.authenticate(username=username, password=password)
            if user:
                if user.is_active:
                    auth.login(request, user)
                    return redirect('home')

                messages.error(request, 'Account is not active!' + user.username)
                return render(request, 'auth/login.html')

            messages.error(request, 'Invalid credentials, try again!')
            return render(request, 'auth/login.html')

    def logout_user(request):
        logout(request)
        return HttpResponseRedirect('/')


class ItemView(View):

    def manage_item(request):
        items = Item.objects.all()
        context = {
            'items': items
        }
        return render(request, 'item/index.html', context)

    def add_item(request):
        item_types = ItemType.objects.all()
        context = {
            'item_types': item_types
        }
        return render(request, "item/add.html", context)

    def save_item(request):
        if request.method != "POST":
            messages.error(request, "Invalid Method!")
            return redirect('add_item')
        else:
            item_name = request.POST.get('item_name')
            item_name_kh = request.POST.get('item_name_kh')
            item_type_id = request.POST.get('item_type_id')
            try:
                item_model = Item(item_name=item_name, item_name_kh=item_name_kh, item_type_id=item_type_id)
                item_model.save()
                messages.success(request, "Insert data successfully!")
                return redirect('item')
            except:
                messages.error(request, "Failed to insert data!")
                return redirect('item')

    def edit_item(request, id):
        items = Item.objects.get(id=id)
        item_types = ItemType.objects.all()
        context = {
            "id": id,
            "items": items,
            'item_types': item_types
        }
        return render(request, "item/edit.html", context)

    def update_item(request):
        if request.method != "POST":
            HttpResponse("Invalid Method")
        else:
            id = request.POST.get('id')
            item_name = request.POST.get('item_name')
            item_name_kh = request.POST.get('item_name_kh')
            item_type_id = request.POST.get('item_type_id')

            try:
                item = Item.objects.get(id=id)
                item.item_name = item_name
                item.item_name_kh = item_name_kh
                item.item_type_id = item_type_id
                item.save()

                messages.success(request, "Updated data successfully.")
                return redirect('item')

            except:
                messages.error(request, "Failed to Update data!")
                return redirect('item')

    def delete_item(request, id):
        items = Item.objects.get(id=id)
        context = {
            "id": id,
            "items": items
        }
        return render(request, "item/delete.html", context)

    def remove_item(request):
        id = request.POST.get('id')
        item = Item.objects.get(id=id)
        try:
            item.delete()
            messages.success(request, "Deleted data successfully.")
            return redirect('item')
        except:
            messages.error(request, "Failed to Delete data.")
            return redirect('item')


class ItemTypeView(View):

    def manage_item_type(request):
        item_types = ItemType.objects.all()
        context = {
            'item_types': item_types
        }
        return render(request, 'item_type/index.html', context)

    def add_item_type(request):
        return render(request, "item_type/add.html")

    def edit_item_type(request, id):
        item_types = ItemType.objects.get(id=id)
        context = {
            "id": id,
            "item_types": item_types
        }
        return render(request, "item_type/edit.html", context)

    def update_item_type(request):
        if request.method != "POST":
            HttpResponse("Invalid Method")
        else:
            item_type_id = request.POST.get('id')
            item_type_name = request.POST.get('item_type_name')
            item_type_name_kh = request.POST.get('item_type_name_kh')

            try:
                item_type = ItemType.objects.get(id=item_type_id)
                item_type.item_type_name = item_type_name
                item_type.item_type_name_kh = item_type_name_kh
                item_type.save()

                messages.success(request, "Course Updated Successfully.")
                return redirect('/item_type/')

            except:
                messages.error(request, "Failed to Update Course.")
                return redirect('/edit_item_type/' + item_type_id)

    def save_item_type(request):
        if request.method != "POST":
            messages.error(request, "Invalid Method!")
            return redirect('add_course')
        else:
            item_type_name = request.POST.get('item_type_name')
            item_type_name_kh = request.POST.get('item_type_name_kh')
            try:
                course_model = ItemType(item_type_name=item_type_name, item_type_name_kh=item_type_name_kh)
                course_model.save()
                messages.success(request, "Course Added Successfully!")
                return redirect('item_type')
            except:
                messages.error(request, "Failed to Add Course!")
                return redirect('add_item_type')

    def delete_item_type(request, id):
        item_types = ItemType.objects.get(id=id)
        context = {
            "id": id,
            "item_types": item_types
        }
        return render(request, "item_type/delete.html", context)

    def remove_item_type(request):
        item_type_id = request.POST.get('id')
        item_type = ItemType.objects.get(id=item_type_id)
        try:
            item_type.delete()
            messages.success(request, "Course Deleted Successfully.")
            return redirect('item_type')
        except:
            messages.error(request, "Failed to Delete Course.")
            return redirect('delete_item_type')
        
        
class ReportView(View):
    
    def invoice (request):
        return render(request, 'report/invoice.html')
        
        