from django.db import models


class ItemType(models.Model):
    id = models.AutoField(primary_key=True)
    item_type_name = models.CharField(max_length=255)
    item_type_name_kh = models.CharField(max_length=255)
    objects = models.Manager()


class Item(models.Model):
    id = models.AutoField(primary_key=True)
    item_name = models.CharField(max_length=255)
    item_name_kh = models.CharField(max_length=255)
    item_type = models.ForeignKey(ItemType, on_delete=models.CASCADE,default=1)
    objects = models.Manager()