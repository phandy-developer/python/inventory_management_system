
from django.urls import path

from APP import views

urlpatterns = [
    path('', views.LoginView.get, name="login"),
    path('do_login/', views.LoginView.post, name="do_login"),
    path('logout/', views.LoginView.logout_user, name="logout"),

    # home
    path('home/', views.home, name="home"),

    # Item Type
    path('item_type/', views.ItemTypeView.manage_item_type, name="item_type"),
    path('add_item_type/', views.ItemTypeView.add_item_type, name="add_item_type"),
    path('save_item_type/', views.ItemTypeView.save_item_type, name="save_item_type"),
    path('edit_item_type/<id>/', views.ItemTypeView.edit_item_type, name="edit_item_type"),
    path('update_item_type/', views.ItemTypeView.update_item_type, name="update_item_type"),
    path('delete_item_type/<id>/', views.ItemTypeView.delete_item_type, name="delete_item_type"),
    path('remove_item_type/', views.ItemTypeView.remove_item_type, name="remove_item_type"),

    # Item
    path('item/', views.ItemView.manage_item, name="item"),
    path('add_item/', views.ItemView.add_item, name="add_item"),
    path('save_item/', views.ItemView.save_item, name="save_item"),
    path('edit_item/<id>', views.ItemView.edit_item, name="edit_item"),
    path('update_item/', views.ItemView.update_item, name="update_item"),
    path('delete_item/<id>/', views.ItemView.delete_item, name="delete_item"),
    path('remove_item/', views.ItemView.remove_item, name="remove_item"),
    
    #Report
    path('invoice/', views.ReportView.invoice, name="invoice"),
]
