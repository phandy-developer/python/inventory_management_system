$(document).ready(function () {
  $(function () {
    $("#example1")
      .DataTable({
        responsive: true,
        lengthChange: true,
        autoWidth: false,

        // buttons: {
        //   buttons: ["copy", "csv", "excel", "pdf", "print", "colvis"],
        //   dom: {
        //     button: {
        //       className: "btn btn-primary",
        //     },
        //     buttonLiner: {
        //       tag: null,
        //     },
        //   },
        // },
      })
      .buttons()
      .container()
      .appendTo("#example1_wrapper .col-md-6:eq(0)");
  });
});
